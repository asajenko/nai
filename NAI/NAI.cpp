#include "stdafx.h"
#include <sstream>
#include <string>
#include <iostream>
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <fstream>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\objdetect\objdetect.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\video\tracking.hpp>
#include <opencv2\ml\ml.hpp>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>



using namespace cv;
using namespace std;
//initial min and max HSV filter values.
//these will be changed using trackbars
int H_MIN = 0;
int H_MAX = 256;
int S_MIN = 0;
int S_MAX = 256;
int V_MIN = 0;
int V_MAX = 256;
//default capture width and height
const int FRAME_WIDTH = 800;
const int FRAME_HEIGHT = 600;
//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS = 50;
//minimum and maximum object area
const int MIN_OBJECT_AREA = 20 * 20;
const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH / 1.5;
//names that will appear at the top of each window
const string windowName = "Original Image";

string intToString(int number){


	std::stringstream ss;
	ss << number;
	return ss.str();
}

void morphOps(Mat &thresh){
	Mat erodeElement = getStructuringElement(MORPH_RECT, Size(3, 3));
	Mat dilateElement = getStructuringElement(MORPH_RECT, Size(8, 8));
	//Erode usuwa prostok�ty o rozmiarze 3x3px dooko�a wykrytego elementu
	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	//rozszerza pozosta�e elementy o 8px
	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);
}
void trackFilteredObject(int &x, int &y, Mat threshold, Mat &cameraFeed){
	Mat temp;
	threshold.copyTo(temp);
	vector< vector<Point> > contours;
	vector<Vec4i> hierarchy;
	//wyszukanie konturu na obrazie binarnym - cv-external - zewn�trzne kontury
	findContours(temp, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	double refArea = 0;
	bool objectFound = false;
	if (hierarchy.size() > 0) {
		int numObjects = hierarchy.size();
		//je�eli wykryty obiekt - czy obraz jest czysty, czy ten kontur
		if (numObjects<MAX_NUM_OBJECTS){
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {

				//Wyznaczenie wektora cech obiektu - obwod, powierzchnia, �rodek
				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;
				if (area>MIN_OBJECT_AREA && area<MAX_OBJECT_AREA && area>refArea){
					x = moment.m10 / area;
					y = moment.m01 / area;
					objectFound = true;
					refArea = area;
				}
				else objectFound = false;


			}
			if (objectFound == true){
				//rysujemy punkt rysuj�cy
				circle(cameraFeed, Point(x, y), 10, Scalar(0, 0, 255), -1);

			}

		}
	}
}

void image_to_vector(Mat & vector, const Mat & img, int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			vector.at<float>(0, i * rows + j) = img.at<uchar>(i, j) / 255.0;
		}
	}
}

int main(int argc, char* argv[])
{
	int lastX = -1;
	int lastY = -1;
	bool trackObjects = true;
	bool useMorphOps = true;
	//Macierz do obrazu przechwyconego z kamery
	Mat cameraFeed;
	//Macierz z filtrami HSV - odcie�, nasycenie, warto��
	Mat HSV;
	//Macierz obrazu w formie zero jedynkowej (binaryzacja)
	Mat thresholds;
	
	int x = 0, y = 0;

	//Obiekt obs�ugi kamery
	VideoCapture capture;
	//Uruchomienie domy�lnej systemowej kamery
	capture.open(0);
	//Ustawienie szeroko�ci i wysoko�ci przechwytywanego obrazu z kamery
	capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
	//odczyt obrazu z kamery do tymczasowego obiektu
	Mat imgTmp;
	capture.read(imgTmp);
	//Tworzenie macierzy zapisanej zerami o wielko�ci obrazu, zapisana na 8 bitach w 3 kana�ach kolor�w BGR
	Mat imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);;

	int k = 0;

	int n_rows = 16, n_cols = 16;            //Rozmiary danych trenujacych 
	int size = n_rows * n_cols;        //Rozmiary wektora trenujacego 

	const string file_name = "semeion.data";
	//const string file_name = "train-images.idx3-ubyte";
	std::fstream file;
	file.open(file_name.c_str(), std::ios::in);
	//stworzenie macierzy vektor�w treningowych o wielko�ci 32 bit�w z jednym kana�em kolory
	Mat trainingvector(1, size, CV_32FC1);
	Mat traininglabels(1, 1, CV_32FC1);
	float buf;
	unsigned char label_catch;
	float label;
	size_t index = -1;
	while (!file.eof())
	{
		index++;
		if (index != 0)
		{
			traininglabels.resize(index + 1);
			trainingvector.resize(index + 1);
		}
		for (int i = 0; i < size; i++)
		{
			file >> buf;
			trainingvector.at<float>(index, i) = buf;
		}
		for (int i = 0; i < 10; i++)
		{
			file >> label_catch;
			if (label_catch == '1') label = float(i);
		}
		traininglabels.at<float>(index, 0) = label;
	}
	traininglabels.resize(index);    //Obci�cie pustego wiersza 
	trainingvector.resize(index);    //kt�ry pojawia si� ze wzgl�du na konstrukcj� p�tli 

	//Stworzenie klasyfikatora obiekt�w - algorytm knearest
	KNearest knn(trainingvector, traininglabels);

	bool detecting_color = true;
	Vec3b hsv_color, rgb_color;
	string result = "";

	while (1){
		//zapisanie obrazu do macierzy
		capture.read(cameraFeed);
		//odwr�cenie obracu o 90 stopni
		flip(cameraFeed, cameraFeed, 1);
		//konwertowanie z BGR do HSV
		cvtColor(cameraFeed, HSV, COLOR_BGR2HSV);
		 
		if (detecting_color == true) {
			//przechwycanie koloru rgb tylko do wy�wietlenia na ekranie
			rgb_color = cameraFeed.at<Vec3b>(Point(320, 240));
			//przechwycenie koloru HSV do in range
			hsv_color = HSV.at<Vec3b>(Point(320, 240));
			//ko�o namierzaj�ce
			circle(cameraFeed, Point(320, 240), 10, Scalar(0, 0, 255), 5, CV_AA);
			//ko�o wy�wietlajace
			circle(cameraFeed, Point(590, 430), 30, Scalar((int)rgb_color.val[0], (int)rgb_color.val[1], (int)rgb_color.val[2]), CV_FILLED, CV_AA);
			putText(cameraFeed, "Namierz kolor i kliknij spacje", Point(20, 30), FONT_HERSHEY_COMPLEX, 0.4, Scalar(0, 255, 0), 1, CV_AA, false);
			imshow(windowName, cameraFeed);
			if (waitKey(15) == 32) detecting_color = false;
			continue;
		}

		//z HSV pobieramy pobieramy obiekty o kolorze z danych zakres�w i zapisujemy do binarnego tresholda
		inRange(HSV, Scalar(hsv_color.val[0] - 30, hsv_color.val[1] - 30, hsv_color.val[2] - 30), Scalar(hsv_color.val[0] + 30, hsv_color.val[1] + 30, hsv_color.val[2] + 30), thresholds);
		//wygladzanie obrazu
		if (useMorphOps)
			morphOps(thresholds);

		if (trackObjects)
		{
			//sledzimy obiekt
			trackFilteredObject(x, y, thresholds, cameraFeed);
			//rysujemy linie
			line(imgLines, Point(x, y), Point(lastX, lastY), Scalar(255, 255, 255), 10);
		}


		lastX = x;
		lastY = y;

		cameraFeed = cameraFeed + imgLines;
		putText(cameraFeed, "Pokazano: " + result, Point(500, 30), FONT_HERSHEY_COMPLEX, 0.4, Scalar(0, 255, 255), 1, CV_AA, false);
		//show frames 
		imshow(windowName, cameraFeed);
	
		if (GetAsyncKeyState(VK_RETURN))
		{
			detecting_color = true;
			imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);
		}

		if (GetAsyncKeyState(VK_DELETE))
		{
			imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);
		}

		
		if (GetAsyncKeyState(VK_RIGHT))
		{
			Mat digit;
			vector<vector<Point>> contours;
			vector<Point> contours_poly;
			Rect boundRect, newRect;
			Mat cont, img_gray;
			cvtColor(imgLines, cont, CV_BGR2GRAY);
			cvtColor(imgLines, img_gray, CV_BGR2GRAY);
			//szukanie konturu narysowanego obiektu
			findContours(cont, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
			for (unsigned i = 0; i < contours.size(); i++)
			{
				//w img lines rysujemy kontury
				drawContours(imgLines, contours, i, Scalar(125, 125, 250), 2);
			}
			//obrysowanie punkt�w kontur�w - uzyskanie wielokatu
			approxPolyDP(Mat(contours[0]), contours_poly, 3, true);
			//opisanie prostokatu na wielokacie - zwocenie LG pkt + dl i szer
			boundRect = boundingRect(Mat(contours_poly));
			//wyciecie obiektu z obrazu z odcieniami szaro�ci na podstawie powy�szych danych
			digit = img_gray(boundRect);
			//zmienia wyciety obraz do romiarow danych trenignowcyh
			resize(digit, digit, Size(n_rows, n_cols), 1.0, 1.0);
			//zapisaie wycietego obrazu w zero jedynkach
			threshold(digit, digit, 1, 255, CV_THRESH_BINARY);

			Mat testvector(1, size, CV_32FC1);
			image_to_vector(testvector, digit, n_rows, n_cols);
			
			Mat pred(1, 1, CV_32FC1);
			knn.find_nearest(testvector, 1, &pred);
			std::stringstream out;
			out << pred.at<float>(0, 0);
			result = out.str();
			cout << out.str() << endl;;
			imgLines = Mat::zeros(imgTmp.size(), CV_8UC3);
			k = 0;
			waitKey(1000);
		}
		waitKey(30);
		k++;
	}
	return 0;
}